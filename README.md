# 安车通实时报警小程序（后端）

#### 介绍
微信小程序
实现功能：汽车在行驶的过程中如果前方遇到限高杆，车前的摄像头在安全距离前测出限高杆的高度后，小程序能够实时报警（当前车辆内否安全通过）

#### 软件架构
后端使用springboot+mybatis+websocket


#### 安装教程

1.  将sql文件导入自己数据库（先创建一个名为:weixin_car的数据库，导入文件即可）
2.  将本项目导入idea，更改yaml文件里的数据库相关配置，直接启动即可
#### 项目演示

![输入图片说明](https://images.gitee.com/uploads/images/2022/0224/202016_a9259d67_8020762.jpeg "179dfcf11416ce558283b451155c972.jpg")
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
