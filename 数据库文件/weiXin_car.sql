/*
SQLyog Enterprise v12.09 (64 bit)
MySQL - 8.0.24 : Database - weixin_car
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`weixin_car` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

/*Table structure for table `user_car` */

DROP TABLE IF EXISTS `user_car`;

CREATE TABLE `user_car` (
  `user_name` varchar(11) NOT NULL COMMENT '用户名',
  `car_num` varchar(11) NOT NULL COMMENT '车牌号',
  `is_owner` tinyint DEFAULT NULL COMMENT '1:拥有 0:不拥有',
  `is_msg` tinyint DEFAULT '0' COMMENT '1:开启 0：未开启',
  PRIMARY KEY (`user_name`,`car_num`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `user_car` */

insert  into `user_car`(`user_name`,`car_num`,`is_owner`,`is_msg`) values ('太里皮','CSDHCGV',1,0),('无畏-何谓','ABCVD',1,0),('无畏-何谓','ACSVTV',0,0),('无畏-何谓','CDSBH',1,0),('无畏-何谓','CSDVM',1,0),('离谱','CDSBH',0,0),('离谱','CSBCH',1,0),('离谱','CSDCC',1,0);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
